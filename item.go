package main

import "time"

type item struct {
	id int
	from time.Time
	to time.Time
	company string
	languages []string
	description string
}
