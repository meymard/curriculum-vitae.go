package cmd

import (
	"github.com/spf13/cobra"
)

var itemCmd = &cobra.Command{
	Use:   "item",
	Short: "Manage CV items",
	Long: `Manage CV items
	Add, update or remove items.`,
	// Uncomment the following line if your bare application
	// has an action associated with it:
	// Run: func(cmd *cobra.Command, args []string) { },
}

func GetItemCmd() *cobra.Command {
	return itemCmd
}

func init() {
	rootCmd.AddCommand(itemCmd)
}
