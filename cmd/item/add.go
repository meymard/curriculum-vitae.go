/*
Copyright © 2023 NAME HERE <EMAIL ADDRESS>

*/
package item

import (
	"github.com/spf13/cobra"
	"framagit.org/meymard/curriculum-vitae.go/cmd"
)



var addCmd = &cobra.Command{
	Use:   "add",
	Short: "Add item",
	Long: `Create an item`,
	// Uncomment the following line if your bare application
	// has an action associated with it:
	// Run: func(cmd *cobra.Command, args []string) { },
}

func init() {
	cmd.GetItemCmd().AddCommand(addCmd)
}

